/*
 * Based on the Arduino example code
 */

#define TRIGGER_PIN 30
#define ECHO_PIN 3
#define SOUND_SPEED 0.00034

void resetTrigger(void) {
  //setting the triggerPin to LOW will clear it
  digitalWrite(TRIGGER_PIN, LOW);
}

void setup() {
  pinMode(TRIGGER_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  Serial.begin(9600);
}

void loop() {
  long ms;
  double dist;
  
  resetTrigger();
  delayMicroseconds(2);

  // need to have a 10ms pulse to activate the trigger pin
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  resetTrigger();
  
  ms = pulseIn(ECHO_PIN, HIGH);
  
  // calculate the distance which involves dividing by 2 as it has to go there and back
  // and then dividing by the speed of sound (in m/s) - actually multiplied as it's
  // stored as the inverse.
  dist = ms*SOUND_SPEED/2;
  
  Serial.print("Distance(cm): ");
  Serial.println(dist);
}
