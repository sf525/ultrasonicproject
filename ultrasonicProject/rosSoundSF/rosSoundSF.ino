/* 
 * based on the rosserial Ultrasound example for the Maxbotix Ultrasound rangers.
 * 
 * Also using: https://answers.ros.org/question/189073/rostopic-pub-multiarray/
 * and: https://gist.github.com/alexsleat/1372845/7e39518cfa12ac91aca4378843e55862eb9ed41d
 * 
 * Modified by SF to work with the HCSR04 ultrasonic sensor
 */

#include <ros.h>
#include <ros/time.h>
#include <sensor_msgs/Range.h>
#include <std_msgs/Int8MultiArray.h>

#define TRIGGER 30
#define ECHO 3
#define SOUND_SPEED 0.00034

// Port numbers for the LEDs
#define RED 31
#define AMBER 32
#define GREEN 33

ros::NodeHandle  nh;

sensor_msgs::Range range_msg;
ros::Publisher pub_range( "/ultrasound", &range_msg);

void ragMessage(const std_msgs::Int8MultiArray& ragRanges_msg) {
  if (ragRanges_msg.data[0] > 0) {
    digitalWrite(RED, HIGH);
  } else {
    digitalWrite(RED, LOW);
  }

  if (ragRanges_msg.data[1] > 0) {
    digitalWrite(AMBER, HIGH);
  } else {
    digitalWrite(AMBER, LOW);
  }

  if (ragRanges_msg.data[2] > 0) {
    digitalWrite(GREEN, HIGH);
  } else {
    digitalWrite(GREEN, LOW);
  }
  
  return;
}

ros::Subscriber<std_msgs::Int8MultiArray> subRagRanges( "/ragRanges", &ragMessage);

char frameid[] = "/ultrasound";

float getRangeUltrasound(){
  long ms;
  float dist;
  digitalWrite(TRIGGER, LOW);
  delayMicroseconds(2);
  // need to have a 10ms pulse to activate the trigger pin
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER, LOW);
  ms = pulseIn(ECHO, HIGH);
  
  // calculate the distance which involves dividing by 2 as it has to go there and back
  // and then dividing by the speed of sound (in cm/s)
  dist = ms*SOUND_SPEED/2;
  
  return dist;   // (0.0124023437 /4) ; //cvt to meters
}

void setup()
{
  nh.initNode();
  nh.advertise(pub_range);
  nh.subscribe(subRagRanges);
  
  range_msg.radiation_type = sensor_msgs::Range::ULTRASOUND; // results in 0
  range_msg.header.frame_id =  frameid; // this is the name of the topic
  range_msg.field_of_view = 0.1;  // fake
  
  pinMode(TRIGGER,OUTPUT);
  pinMode(ECHO,INPUT);
  pinMode(RED,OUTPUT);
  pinMode(AMBER,OUTPUT);
  pinMode(GREEN,OUTPUT);
  digitalWrite(RED, LOW);
  digitalWrite(AMBER, LOW);
  digitalWrite(GREEN, LOW);
}

void loop()
{
  float range;
  range = getRangeUltrasound();
  range_msg.min_range = range * 1.2; //this is used as the dummy left ultrasonic mounted to point 30 deg left
  range_msg.range = range; // this is the genuine range sensor pointing straight ahead
  range_msg.max_range = range * 0.8; //this is used as the dummy right ultrasonic mounted to point 30 deg right
  range_msg.header.stamp = nh.now();
  pub_range.publish(&range_msg);

  nh.spinOnce();
}
