#!/usr/bin/env python
# code used from: https://answers.ros.org/question/9471/how-to-recieve-an-array-over-publisher-and-subscriber-python/
# and: http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29
#
# modified by SFO to fit my ultrasonic project for PDE4420-1

from std_msgs.msg import Int8MultiArray, MultiArrayLayout, MultiArrayDimension
from sensor_msgs.msg import Range
import rospy

RED_THRESHOLD = 0.05
AMBER_THRESHOLD = 0.1

class RagRange:
    def __init__(self):
        self.layout = MultiArrayLayout()
        self.layout.dim = [MultiArrayDimension()]
        self.layout.dim[0].label = 'ragRanges'
        self.layout.dim[0].size = 3
        self.layout.dim[0].stride = 1
        self.layout.data_offset = 0
        self.dim = [0,0,0] # these numbers are red, amber, green

def rangeCallback(data):
    global ragRange
    rospy.loginfo(data)
    if min([data.min_range, data.range, data.max_range]) < RED_THRESHOLD:
        ragRange.dim = [1,0,0]
    elif min([data.min_range, data.range, data.max_range]) < AMBER_THRESHOLD:
        ragRange.dim = [0,1,0]
    else:
        ragRange.dim = [0,0,1]

rospy.init_node('ragUltra', anonymous=True)
ragPub = rospy.Publisher("ragRanges", Int8MultiArray, queue_size=10)
ragSub = rospy.Subscriber("ultrasound", Range, rangeCallback)

rate = rospy.Rate(10) # 10hz
ragRange = RagRange()

while not rospy.is_shutdown():
    # layout = MultiArrayLayout()
    # layout.dim = [MultiArrayDimension()]
    # layout.dim[0].label = 'ragRanges'
    # layout.dim[0].size = 3
    # layout.dim[0].stride = 1
    # layout.data_offset = 0
    # dim = [1,1,0]
    # rospy.loginfo(layout)
    # rospy.loginfo(dim)
    ragPub.publish(ragRange.layout,ragRange.dim)
    rate.sleep()
