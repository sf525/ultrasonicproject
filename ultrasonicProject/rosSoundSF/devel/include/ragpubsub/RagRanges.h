// Generated by gencpp from file ragpubsub/RagRanges.msg
// DO NOT EDIT!


#ifndef RAGPUBSUB_MESSAGE_RAGRANGES_H
#define RAGPUBSUB_MESSAGE_RAGRANGES_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace ragpubsub
{
template <class ContainerAllocator>
struct RagRanges_
{
  typedef RagRanges_<ContainerAllocator> Type;

  RagRanges_()
    : ragRanges()  {
      ragRanges.assign(0);
  }
  RagRanges_(const ContainerAllocator& _alloc)
    : ragRanges()  {
  (void)_alloc;
      ragRanges.assign(0);
  }



   typedef boost::array<int8_t, 3>  _ragRanges_type;
  _ragRanges_type ragRanges;





  typedef boost::shared_ptr< ::ragpubsub::RagRanges_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::ragpubsub::RagRanges_<ContainerAllocator> const> ConstPtr;

}; // struct RagRanges_

typedef ::ragpubsub::RagRanges_<std::allocator<void> > RagRanges;

typedef boost::shared_ptr< ::ragpubsub::RagRanges > RagRangesPtr;
typedef boost::shared_ptr< ::ragpubsub::RagRanges const> RagRangesConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::ragpubsub::RagRanges_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::ragpubsub::RagRanges_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace ragpubsub

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'ragpubsub': ['/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::ragpubsub::RagRanges_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::ragpubsub::RagRanges_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::ragpubsub::RagRanges_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::ragpubsub::RagRanges_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::ragpubsub::RagRanges_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::ragpubsub::RagRanges_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::ragpubsub::RagRanges_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ffa71a4b7f296c2f1f65a51cef564cf5";
  }

  static const char* value(const ::ragpubsub::RagRanges_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xffa71a4b7f296c2fULL;
  static const uint64_t static_value2 = 0x1f65a51cef564cf5ULL;
};

template<class ContainerAllocator>
struct DataType< ::ragpubsub::RagRanges_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ragpubsub/RagRanges";
  }

  static const char* value(const ::ragpubsub::RagRanges_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::ragpubsub::RagRanges_<ContainerAllocator> >
{
  static const char* value()
  {
    return "#int8 red # three ints used to control the red, amber, green\n\
#int8 amber \n\
#int8 green\n\
int8[3] ragRanges # an array of three ints used to control the red, amber, green\n\
";
  }

  static const char* value(const ::ragpubsub::RagRanges_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::ragpubsub::RagRanges_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.ragRanges);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct RagRanges_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::ragpubsub::RagRanges_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::ragpubsub::RagRanges_<ContainerAllocator>& v)
  {
    s << indent << "ragRanges[]" << std::endl;
    for (size_t i = 0; i < v.ragRanges.size(); ++i)
    {
      s << indent << "  ragRanges[" << i << "]: ";
      Printer<int8_t>::stream(s, indent + "  ", v.ragRanges[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // RAGPUBSUB_MESSAGE_RAGRANGES_H
