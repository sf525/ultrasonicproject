
(cl:in-package :asdf)

(defsystem "ragpubsub-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "RagRanges" :depends-on ("_package_RagRanges"))
    (:file "_package_RagRanges" :depends-on ("_package"))
  ))