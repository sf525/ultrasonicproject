; Auto-generated. Do not edit!


(cl:in-package ragpubsub-msg)


;//! \htmlinclude RagRanges.msg.html

(cl:defclass <RagRanges> (roslisp-msg-protocol:ros-message)
  ((ragRanges
    :reader ragRanges
    :initarg :ragRanges
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 3 :element-type 'cl:fixnum :initial-element 0)))
)

(cl:defclass RagRanges (<RagRanges>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RagRanges>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RagRanges)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ragpubsub-msg:<RagRanges> is deprecated: use ragpubsub-msg:RagRanges instead.")))

(cl:ensure-generic-function 'ragRanges-val :lambda-list '(m))
(cl:defmethod ragRanges-val ((m <RagRanges>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ragpubsub-msg:ragRanges-val is deprecated.  Use ragpubsub-msg:ragRanges instead.")
  (ragRanges m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RagRanges>) ostream)
  "Serializes a message object of type '<RagRanges>"
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    ))
   (cl:slot-value msg 'ragRanges))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RagRanges>) istream)
  "Deserializes a message object of type '<RagRanges>"
  (cl:setf (cl:slot-value msg 'ragRanges) (cl:make-array 3))
  (cl:let ((vals (cl:slot-value msg 'ragRanges)))
    (cl:dotimes (i 3)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RagRanges>)))
  "Returns string type for a message object of type '<RagRanges>"
  "ragpubsub/RagRanges")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RagRanges)))
  "Returns string type for a message object of type 'RagRanges"
  "ragpubsub/RagRanges")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RagRanges>)))
  "Returns md5sum for a message object of type '<RagRanges>"
  "ffa71a4b7f296c2f1f65a51cef564cf5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RagRanges)))
  "Returns md5sum for a message object of type 'RagRanges"
  "ffa71a4b7f296c2f1f65a51cef564cf5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RagRanges>)))
  "Returns full string definition for message of type '<RagRanges>"
  (cl:format cl:nil "#int8 red # three ints used to control the red, amber, green~%#int8 amber ~%#int8 green~%int8[3] ragRanges # an array of three ints used to control the red, amber, green~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RagRanges)))
  "Returns full string definition for message of type 'RagRanges"
  (cl:format cl:nil "#int8 red # three ints used to control the red, amber, green~%#int8 amber ~%#int8 green~%int8[3] ragRanges # an array of three ints used to control the red, amber, green~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RagRanges>))
  (cl:+ 0
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'ragRanges) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RagRanges>))
  "Converts a ROS message object to a list"
  (cl:list 'RagRanges
    (cl:cons ':ragRanges (ragRanges msg))
))
