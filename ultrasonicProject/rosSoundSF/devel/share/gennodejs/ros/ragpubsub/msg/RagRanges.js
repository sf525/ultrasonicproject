// Auto-generated. Do not edit!

// (in-package ragpubsub.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class RagRanges {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.ragRanges = null;
    }
    else {
      if (initObj.hasOwnProperty('ragRanges')) {
        this.ragRanges = initObj.ragRanges
      }
      else {
        this.ragRanges = new Array(3).fill(0);
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RagRanges
    // Check that the constant length array field [ragRanges] has the right length
    if (obj.ragRanges.length !== 3) {
      throw new Error('Unable to serialize array field ragRanges - length must be 3')
    }
    // Serialize message field [ragRanges]
    bufferOffset = _arraySerializer.int8(obj.ragRanges, buffer, bufferOffset, 3);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RagRanges
    let len;
    let data = new RagRanges(null);
    // Deserialize message field [ragRanges]
    data.ragRanges = _arrayDeserializer.int8(buffer, bufferOffset, 3)
    return data;
  }

  static getMessageSize(object) {
    return 3;
  }

  static datatype() {
    // Returns string type for a message object
    return 'ragpubsub/RagRanges';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ffa71a4b7f296c2f1f65a51cef564cf5';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #int8 red # three ints used to control the red, amber, green
    #int8 amber 
    #int8 green
    int8[3] ragRanges # an array of three ints used to control the red, amber, green
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RagRanges(null);
    if (msg.ragRanges !== undefined) {
      resolved.ragRanges = msg.ragRanges;
    }
    else {
      resolved.ragRanges = new Array(3).fill(0)
    }

    return resolved;
    }
};

module.exports = RagRanges;
