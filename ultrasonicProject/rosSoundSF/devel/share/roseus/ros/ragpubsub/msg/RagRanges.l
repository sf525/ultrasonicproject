;; Auto-generated. Do not edit!


(when (boundp 'ragpubsub::RagRanges)
  (if (not (find-package "RAGPUBSUB"))
    (make-package "RAGPUBSUB"))
  (shadow 'RagRanges (find-package "RAGPUBSUB")))
(unless (find-package "RAGPUBSUB::RAGRANGES")
  (make-package "RAGPUBSUB::RAGRANGES"))

(in-package "ROS")
;;//! \htmlinclude RagRanges.msg.html


(defclass ragpubsub::RagRanges
  :super ros::object
  :slots (_ragRanges ))

(defmethod ragpubsub::RagRanges
  (:init
   (&key
    ((:ragRanges __ragRanges) (make-array 3 :initial-element 0 :element-type :integer))
    )
   (send-super :init)
   (setq _ragRanges __ragRanges)
   self)
  (:ragRanges
   (&optional __ragRanges)
   (if __ragRanges (setq _ragRanges __ragRanges)) _ragRanges)
  (:serialization-length
   ()
   (+
    ;; int8[3] _ragRanges
    (* 1    3)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int8[3] _ragRanges
     (dotimes (i 3)
       (write-byte (elt _ragRanges i) s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int8[3] _ragRanges
   (dotimes (i (length _ragRanges))
     (setf (elt _ragRanges i) (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> (elt _ragRanges i) 127) (setf (elt _ragRanges i) (- (elt _ragRanges i) 256)))
     )
   ;;
   self)
  )

(setf (get ragpubsub::RagRanges :md5sum-) "ffa71a4b7f296c2f1f65a51cef564cf5")
(setf (get ragpubsub::RagRanges :datatype-) "ragpubsub/RagRanges")
(setf (get ragpubsub::RagRanges :definition-)
      "#int8 red # three ints used to control the red, amber, green
#int8 amber 
#int8 green
int8[3] ragRanges # an array of three ints used to control the red, amber, green

")



(provide :ragpubsub/RagRanges "ffa71a4b7f296c2f1f65a51cef564cf5")


