# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "ragpubsub: 1 messages, 0 services")

set(MSG_I_FLAGS "-Iragpubsub:/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(ragpubsub_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg" NAME_WE)
add_custom_target(_ragpubsub_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "ragpubsub" "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(ragpubsub
  "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/ragpubsub
)

### Generating Services

### Generating Module File
_generate_module_cpp(ragpubsub
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/ragpubsub
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(ragpubsub_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(ragpubsub_generate_messages ragpubsub_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg" NAME_WE)
add_dependencies(ragpubsub_generate_messages_cpp _ragpubsub_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(ragpubsub_gencpp)
add_dependencies(ragpubsub_gencpp ragpubsub_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS ragpubsub_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(ragpubsub
  "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/ragpubsub
)

### Generating Services

### Generating Module File
_generate_module_eus(ragpubsub
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/ragpubsub
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(ragpubsub_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(ragpubsub_generate_messages ragpubsub_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg" NAME_WE)
add_dependencies(ragpubsub_generate_messages_eus _ragpubsub_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(ragpubsub_geneus)
add_dependencies(ragpubsub_geneus ragpubsub_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS ragpubsub_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(ragpubsub
  "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/ragpubsub
)

### Generating Services

### Generating Module File
_generate_module_lisp(ragpubsub
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/ragpubsub
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(ragpubsub_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(ragpubsub_generate_messages ragpubsub_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg" NAME_WE)
add_dependencies(ragpubsub_generate_messages_lisp _ragpubsub_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(ragpubsub_genlisp)
add_dependencies(ragpubsub_genlisp ragpubsub_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS ragpubsub_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(ragpubsub
  "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/ragpubsub
)

### Generating Services

### Generating Module File
_generate_module_nodejs(ragpubsub
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/ragpubsub
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(ragpubsub_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(ragpubsub_generate_messages ragpubsub_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg" NAME_WE)
add_dependencies(ragpubsub_generate_messages_nodejs _ragpubsub_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(ragpubsub_gennodejs)
add_dependencies(ragpubsub_gennodejs ragpubsub_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS ragpubsub_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(ragpubsub
  "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/ragpubsub
)

### Generating Services

### Generating Module File
_generate_module_py(ragpubsub
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/ragpubsub
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(ragpubsub_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(ragpubsub_generate_messages ragpubsub_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/stu/Documents/arduino/ultrasonicproject/ultrasonicProject/rosSoundSF/src/ragpubsub/msg/RagRanges.msg" NAME_WE)
add_dependencies(ragpubsub_generate_messages_py _ragpubsub_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(ragpubsub_genpy)
add_dependencies(ragpubsub_genpy ragpubsub_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS ragpubsub_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/ragpubsub)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/ragpubsub
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(ragpubsub_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/ragpubsub)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/ragpubsub
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(ragpubsub_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/ragpubsub)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/ragpubsub
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(ragpubsub_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/ragpubsub)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/ragpubsub
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(ragpubsub_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/ragpubsub)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/ragpubsub\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/ragpubsub
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(ragpubsub_generate_messages_py std_msgs_generate_messages_py)
endif()
