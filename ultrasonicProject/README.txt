Stuart Forsyth - M00286624 - PDE4420 - project 1

overview
========
There is an arduino mega microcontroller which has code that publishes sensor data from an ultrasonic sensor to the serial port in such a manner that is in a suitable format for rosserial to convert it into a node to publish the data within ROS.  There is only one physical sensor but it is mocked up as three sensors in total to mimic an array of three sensors that would give a wider range sensor allowing the robot to see from 30 degrees left to 30 degrees right and thus minimise the chance of collisions.
There is a python node which subscribes to the data from the arduino and then publishes data within ROS indicating a RED/AMBER/GREEN state with the intention that this can be subscribed to and an appropriate action taken, for example GREEN = full speed, AMBER = 25% speed, RED = stop and turn (later this could be more advanced according to whether the left, centre or right range sensor breached the threshold).
The arduino subscribes to the ragRanges topic which is published by the python node and for now it simply switches on the appropriately coloured LED but on a more advanced robot it could be used to make decisions and change movement.

folder structure
================
soundSF - this was the original project that I got working on Arduino to make the ultrasonic report on the serial monitor correctly.
rosSoundSF - this is the version for ros that reports correctly and can be viewed with rostopic echo
rosSoundSF/src/ragsubpub - this is the python node that will subscribe to the ultrasonic sensor data being published on the /ultrasound topic and then make a decision about the data to publish on the /ragRanges topic which in turn is subscribed to by the arduino in order to take the appropriate action (which for now is turned on the relevant LED).
note: the rest of the folders are part of the catkin make file structure
